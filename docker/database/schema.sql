CREATE TABLE `debtors` (
 `id` int(5) NOT NULL AUTO_INCREMENT,
 `name` varchar(200) NOT NULL,
 `cpf_cnpj` varchar(18) NOT NULL,
 `birth_date` date NOT NULL,
 `street` varchar(75) NOT NULL,
 `district` varchar(75) NOT NULL,
 `number` int(5) NOT NULL,
 `reference` varchar(100) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1

CREATE TABLE `debts` (
 `id` int(5) NOT NULL AUTO_INCREMENT,
 `description` text NOT NULL,
 `value` decimal(8,2) NOT NULL,
 `due_date` date NOT NULL,
 `updated` datetime DEFAULT NULL,
 `debtor_id` int(5) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_debtors_id` (`debtor_id`),
 CONSTRAINT `fk_debtors_id` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1