#Estrutura do Projeto

* docker
    * app: Contém os Dockerfile do Apache e PHP 
    * database: Contém o Dockerfile do MySQL e do PhpMyadmin, e também o esquema do banco em SQL 
* html
    * src/Daos: Classes de Manipulação de Banco de Dados
    * src/Entitys: Classes de Definição das Entidades da Aplicação
    * src/Lib: Classe que inicializa conexão com banco
    * src/tests: Classes com testes unitários
    * src/views: Arquivos PHP com os códigos HTML
    
    * vendor: Classe gerada pelo composer

#Instruções

Antes de subir o projeto rode um composer install antes.

O docker-compose.yml foi configurado para iniciar o banco automaticamente, caso não inicie o arquivo com a definição do banco está em docker/database/schema.sql 
    
Para rodar o aplicativo basta rodar `$ docker-compose up -d --build```
    
    