<?php

use PHPUnit\Framework\TestCase;
use Src\Entitys\Debtors;
use Src\Daos\DebtorDAO;

class DebtorDAOTest extends TestCase
{

    public function testCanInsertDebtor()
    {
        $debtorDAO = DebtorDAO::getInstance();

        $debtor = new Debtors(
            1,
            'Rodrigo',
            '12854666674',
            '1996-08-20',
            'Rua UM',
            'Biarro um',
            235,
            'ref'
        );

        $this->assertTrue(
            $debtorDAO->insert($debtor)
        );
    }

    public function testCanGetDebtors(){
        $debtorDAO = DebtorDAO::getInstance();

        $this->assertIsArray(
            $debtorDAO->getAll()
        );
    }

    public function testCanDeleteDebtor(){
        $debtorDAO = DebtorDAO::getInstance();
        $debtor = $debtorDAO->getOne(5);

        $this->assertTrue(
            $debtorDAO->delete($debtor->getId())
        );
    }


    public function testCanUpdateDebtor(){
        $debtorDAO = DebtorDAO::getInstance();
        $debtor = $debtorDAO->getOne(6);

        $debtor->setName("Rodrigo");

        $this->assertTrue(
            $debtorDAO->update($debtor)
        );
    }
}
