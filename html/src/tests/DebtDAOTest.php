<?php

use PHPUnit\Framework\TestCase;
use Src\Entitys\Debts;
use Src\Daos\DebtDAO;

class DebtDAOTest extends TestCase
{
    public function testCanInsertDebt(){
        $debtDAO = DebtDAO::getInstance();

        $debt = new Debts(
            1,
            'Divida 456',
            3200.00,
            '2020-08-02',
            '2020-08-02 00:00:00',
            6
        );

        $this->assertTrue(
            $debtDAO->insert($debt)
        );
    }

    public function testCanGetOneDebt(){
        $debtDAO = DebtDAO::getInstance();

        $debtDAO->getOne(1);

        $this->assertClassHasAttribute('value', Debts::class);
    }

    public function testCanUpdateDebt(){
        $debtDAO = DebtDAO::getInstance();

        $debt = $debtDAO->getOne(1);

        $debt->setDescription("Nova descricao");

        $this->assertTrue(
            $debtDAO->update($debt)
        );
    }

    public function testeCangetDebtsFromDebtor(){

        $debtDAO = DebtDAO::getInstance();

        $this->assertIsArray(
          $debtDAO->getDebtsFromDebtor(6)
        );
    }

    public function testCanDeleteDebt(){

        $debtDAO = DebtDAO::getInstance();

        $debt = $debtDAO->getOne(3);

        $this->assertTrue(
            $debtDAO->delete($debt->getId())
        );
    }
}
