<?php

namespace Src\Entitys;

class Debtors
{
    private $id;
    private $name;
    private $cpf_cnpj;
    private $birth_date;
    private $street;
    private $district;
    private $number;
    private $reference;


    public function __construct($id, $name, $cpf_cnpj, $birth_date, $street, $district, $number, $reference)
    {
        $this->id = $id;
        $this->name = $name;
        $this->cpf_cnpj = $cpf_cnpj;
        $this->birth_date = $birth_date;
        $this->street = $street;
        $this->district = $district;
        $this->number = $number;
        $this->reference = $reference;

    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCpfCnpj()
    {
        return $this->cpf_cnpj;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $id
     * @return Debtors
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed $name
     * @return Debtors
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $cpf_cnpj
     * @return Debtors
     */
    public function setCpfCnpj($cpf_cnpj)
    {
        $this->cpf_cnpj = $cpf_cnpj;
        return $this;
    }

    /**
     * @param mixed $birth_date
     * @return Debtors
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
        return $this;
    }

    /**
     * @param mixed $street
     * @return Debtors
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @param mixed $district
     * @return Debtors
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @param mixed $number
     * @return Debtors
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @param mixed $reference
     * @return Debtors
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }


}