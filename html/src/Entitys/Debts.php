<?php

namespace Src\Entitys;

class Debts
{
    private $id;
    private $description;
    private $value;
    private $due_date;
    private $updated;
    private $debtor;

    /**
     * Debts constructor.
     * @param $id
     * @param $description
     * @param $value
     * @param $due_date
     * @param $updated
     * @param $debtor
     */
    public function __construct($id, $description, $value, $due_date, $updated, $debtor)
    {
        $this->id = $id;
        $this->description = $description;
        $this->value = $value;
        $this->due_date = $due_date;
        $this->updated = $updated;
        $this->debtor = $debtor;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Debts
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Debts
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Debts
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->due_date;
    }

    /**
     * @param mixed $due_date
     * @return Debts
     */
    public function setDueDate($due_date)
    {
        $this->due_date = $due_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Debts
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDebtor()
    {
        return $this->debtor;
    }

    /**
     * @param mixed $debtor
     * @return Debts
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
        return $this;
    }




}