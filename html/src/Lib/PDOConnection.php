<?php

namespace Src\Lib;

use PDO;

class PDOConnection
{
    public static $instance;

    public function __construct()
    {
    }

    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new PDO(
                APP_MYSQL_PDO_HOST,
                APP_MYSQL_USER,
                APP_MYSQL_PASSWORD,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
            );
            self::$instance->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }

        return self::$instance;
    }
}