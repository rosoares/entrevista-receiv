<?php
include '../header.php';
include '../sidebar.php';
include '../../../vendor/autoload.php';

use Src\Daos\DebtorDAO;
use Src\Entitys\Debtors;

$debtorDAO = DebtorDAO::getInstance();

$debtor = $debtorDAO->getOne($_GET["id"]);

if (!empty($_POST)){
    $debtor->setName($_POST["name"]);
    $debtor->setCpfCnpj($_POST["cpf_cnpj"]);
    $debtor->setBirthDate($_POST["birth_date"]);
    $debtor->setStreet($_POST["street"]);
    $debtor->setDistrict($_POST["district"]);
    $debtor->setNumber($_POST["number"]);
    $debtor->setReference($_POST["referencec"]);

    if ($debtorDAO->update($debtor) == true){
        $_SESSION['success'] = true;
        $_SESSION['error'] = false;
    } else{
        $_SESSION['error'] = true;
    }
}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Devedores</h1>
    </div>

    <form method="post" action="">
        <?php if ($_SESSION['success']): ?>
            <div class="alert alert-success" role="alert">
                Cadastrado com Sucesso !!!
            </div>
        <?php endif; ?>
        <?php if ($_SESSION['error']): ?>
            <div class="alert alert-danger" role="alert">
                Ocorreu um erro !!!
            </div>
        <?php endif; ?>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Nome</label>
                <input type="text" class="form-control" name="name" placeholder="Seu Nome"
                       value="<?php echo $debtor->getName() ?>" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputPassword4">CPF/CNPJ</label>
                <input type="text" class="form-control" name="cpf_cnpj" placeholder="CPF ou CNPJ"
                       value="<?php echo $debtor->getCpfCnpj() ?>" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress">Data de Nascimento</label>
                <input type="date" class="form-control" name="birth_date"
                       value="<?php echo $debtor->getBirthDate() ?>" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputAddress">Rua</label>
                <input type="text" class="form-control" name="street" placeholder="Rua Exemplo"
                       value="<?php echo $debtor->getStreet() ?>" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Bairro</label>
                <input type="text" class="form-control" name="district" placeholder="Bairro"
                       value="<?php echo $debtor->getDistrict() ?>" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Número</label>
                <input type="text" class="form-control" name="number" placeholder="Número"
                       value="<?php echo $debtor->getNumber() ?>" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputAddress2">Complemento</label>
            <input type="text" class="form-control" name="reference" placeholder="Complemento"
                   value="<?php echo $debtor->getReference() ?>">
        </div>

        <button type="submit" class="btn btn-primary">Atualizar</button>
    </form>