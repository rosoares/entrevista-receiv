<?php
include '../header.php';
include '../sidebar.php';
include '../../../vendor/autoload.php';

use Src\Daos\DebtorDAO;
use Src\Entitys\Debtors;

$debtorDAO = DebtorDAO::getInstance();

if (!empty($_POST)) {
    $debtor = new Debtors(
        null,
        $_POST["name"],
        $_POST["cpf_cnpj"],
        $_POST["birth_date"],
        $_POST["street"],
        $_POST["district"],
        $_POST["number"],
        $_POST["reference"]
    );

    if ($debtorDAO->insert($debtor) == true){
        $_SESSION['success'] = true;
        $_SESSION['error'] = false;
    } else{
        $_SESSION['error'] = true;
    }

}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Devedores</h1>
    </div>

    <form method="post" action="">
        <?php if ($_SESSION['success']): ?>
            <div class="alert alert-success" role="alert">
                Cadastrado com Sucesso !!!
            </div>
        <?php endif; ?>
        <?php if ($_SESSION['error']): ?>
            <div class="alert alert-danger" role="alert">
                Ocorreu um erro !!!
            </div>
        <?php endif; ?>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Nome</label>
                <input type="text" class="form-control" name="name" placeholder="Seu Nome" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputPassword4">CPF/CNPJ</label>
                <input type="text" class="form-control" name="cpf_cnpj" placeholder="CPF ou CNPJ" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress">Data de Nascimento</label>
                <input type="date" class="form-control" name="birth_date" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputAddress">Rua</label>
                <input type="text" class="form-control" name="street" placeholder="Rua Exemplo" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Bairro</label>
                <input type="text" class="form-control" name="district" placeholder="Bairro" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Número</label>
                <input type="text" class="form-control" name="number" placeholder="Número" required>
            </div>
        </div>

        <div class="form-group">
            <label for="inputAddress2">Complemento</label>
            <input type="text" class="form-control" name="reference" placeholder="Complemento">
        </div>

        <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>

