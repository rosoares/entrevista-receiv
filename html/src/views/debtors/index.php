<?php

include '../header.php';
include '../sidebar.php';
include '../../../vendor/autoload.php';

use Src\Daos\DebtorDAO;

$debtorDAO = DebtorDAO::getInstance();

$debtors = $debtorDAO->getAll();

if(!empty($_POST)){
    $debtorDAO->delete($_POST["debtor_id"]);
}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Devedores</h1>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">CPF/CNPJ</th>
            <th scope="col">Data de Nascimento</th>
            <th scope="col">Ação</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($debtors as $debtor): ?>
        <tr>
            <th scope="row"><?php echo $debtor["name"] ?></th>
            <td><?php echo $debtor["cpf_cnpj"] ?></td>
            <td><?php echo $debtor["birth_date"] ?></td>
            <td>
                <form action="" method="post">
                    <a href="../debts/index.php?id=<?php echo $debtor["id"] ?>&debtor=<?php echo $debtor["name"]?>"
                       class="btn btn-warning">Ver Débitos
                    </a>
                    <a href="../debtors/edit.php?id=<?php echo $debtor["id"] ?>" class="btn btn-primary">Editar</a>
                    <button type="submit" class="btn btn-danger">Excluir</button>
                    <input type="hidden" name="debtor_id" value="<?php echo $debtor["id"] ?>">
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <a href="../debtors/create.php" class="btn btn-success">Criar Novo</a>