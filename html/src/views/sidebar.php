<?php



?>
<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link"
                           href="<?php echo 'http://' . $_SERVER['SERVER_NAME'].'/src/views/debts/create.php'?>">
                            <span data-feather="file"></span>
                            Adicionar Débito
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="<?php echo 'http://' . $_SERVER['SERVER_NAME'].'/src/views/debtors/index.php'?>">
                            <span data-feather="shopping-cart"></span>
                            Devedores
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

