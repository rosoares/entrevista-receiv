<?php

include '../header.php';
include '../sidebar.php';
include '../../../vendor/autoload.php';

use Src\Daos\DebtDAO;
use Src\Daos\DebtorDAO;
use Src\Entitys\Debts;

$debtDAO = DebtDAO::getInstance();

$debtorDAO = DebtorDAO::getInstance();

$debtorsList = $debtorDAO->getAll();

if (!empty($_POST)){
    $debt = new Debts(
            null,
        $_POST["description"],
        $_POST["value"],
        $_POST["due_date"],
        null,
        $_POST["debtor"]
    );

    if ($debtDAO->insert($debt) == true){
        $_SESSION['success'] = true;
        $_SESSION['error'] = false;
    } else{
        $_SESSION['error'] = true;
    }

}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Débitos</h1>
    </div>

    <form method="post" action="">
        <?php if ($_SESSION['success']): ?>
            <div class="alert alert-success" role="alert">
                Cadastrado com Sucesso !!!
            </div>
        <?php endif; ?>
        <?php if ($_SESSION['error']): ?>
            <div class="alert alert-danger" role="alert">
                Ocorreu um erro !!!
            </div>
        <?php endif; ?>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputAddress">Devedor</label>
                <select class="form-control" name="debtor" required>
                    <?php foreach ($debtorsList as $debtor): ?>
                        <option value="<?php echo $debtor["id"] ?>"><?php echo $debtor["name"] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Valor</label>
                <input type="text" class="form-control" name="value" placeholder="Valor" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">Vencimento</label>
                <input type="date" class="form-control" name="due_date" placeholder="Vencimento" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Descrição</label>
                <textarea name="description" class="form-control" required></textarea>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>