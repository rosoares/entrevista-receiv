<?php

include '../header.php';
include '../sidebar.php';
include '../../../vendor/autoload.php';

use Src\Daos\DebtDAO;

$debtDAO = DebtDAO::getInstance();

$debts = $debtDAO->getDebtsFromDebtor($_GET["id"]);

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Débitos de <?php echo $_GET["debtor"] ?></h1>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Descrição</th>
            <th scope="col">Valor</th>
            <th scope="col">Vencimento</th>
            <th scope="col">Ação</th>
        </tr>
        </thead>
        <tbody>
        <?php  foreach ($debts as $debt): ?>
        <tr>
            <td><?php echo $debt["description"] ?></td>
            <td><?php echo $debt["value"] ?></td>
            <td><?php echo $debt["due_date"] ?></td>
            <td>
                <a href="../debts/edit.php?id=<?php echo $debt["id"] ?>&debtor=<?php echo $_GET["debtor"] ?>"
                   class="btn btn-primary">Editar
                </a>
                <button class="btn btn-danger">Excluir</button>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>