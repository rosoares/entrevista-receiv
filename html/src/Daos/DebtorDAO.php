<?php

namespace Src\Daos;

use Src\Entitys\Debtors;
use Src\Lib\PDOConnection;
use PDO;

final class DebtorDAO implements DAOInterface
{
    public static $instance;

    public function __construct()
    {
    }

    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new DebtorDAO();

        return self::$instance;
    }

    public function insert($entityObject)
    {
        try {
            $sql = "INSERT INTO debtors (       
                name,
                cpf_cnpj,
                birth_date,
                street,
                district,
                number,
                reference ) 
                VALUES (
                :name,
                :cpf_cnpj,
                :birth_date,
                :street,
                :district,
                :number,
                :reference
                )";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $values = array(
                ':name' => $entityObject->getName(),
                ':cpf_cnpj' => $entityObject->getCpfCnpj(),
                ':birth_date' => $entityObject->getBirthDate(),
                ':street' => $entityObject->getStreet(),
                ':district' => $entityObject->getDistrict(),
                ':number' => $entityObject->getNumber(),
                ':reference' => $entityObject->getReference()
            );

            return $p_sql->execute($values);

        } catch (\PDOException $e) {

            //echo $e->getMessage();
            return false;
        }
    }

    public function getAll()
    {
        try {
            $sql = "SELECT * FROM debtors";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->execute();

            return $p_sql->fetchAll(PDO::FETCH_ASSOC);

        } catch (Exception $e) {

            return $e->getMessage();

        }
    }

    public function getOne($id)
    {
        try {
            $sql = "SELECT * FROM debtors WHERE id = :id";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->bindValue(":id", $id);

            $p_sql->execute();

            $array = $p_sql->fetch(PDO::FETCH_ASSOC);

            return $this->createDebtorFromArray($array);

        } catch (Exception $e) {

            return $e->getMessage();

        }
    }

    public function update($entityObject)
    {
        try {
            $sql = "UPDATE debtors SET       
                name = :name,
                cpf_cnpj = :cpf_cnpj,
                birth_date = :birth_date,
                street = :street,
                district = :district,
                number = :number,
                reference = :reference
                WHERE id = :id
                ";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $values = array(
                ':id' => $entityObject->getId(),
                ':name' => $entityObject->getName(),
                ':cpf_cnpj' => $entityObject->getCpfCnpj(),
                ':birth_date' => $entityObject->getBirthDate(),
                ':street' => $entityObject->getStreet(),
                ':district' => $entityObject->getDistrict(),
                ':number' => $entityObject->getNumber(),
                ':reference' => $entityObject->getReference()
            );

            return $p_sql->execute($values);

        } catch (\PDOException $e) {

            return false;


        }
    }

    public function delete($id)
    {
        try {

            $sql = "DELETE FROM debtors WHERE id = :id";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->bindValue(":id", $id);

            $p_sql->execute();

            return $p_sql->execute();

        } catch (Exception $e) {
            return false;
        }
    }

    private function createDebtorFromArray($array){

        $debtor = new Debtors(
            $array["id"],
            $array["name"],
            $array["cpf_cnpj"],
            $array["birth_date"],
            $array["street"],
            $array["district"],
            $array["number"],
            $array["reference"]
        );
        return $debtor;
    }
}