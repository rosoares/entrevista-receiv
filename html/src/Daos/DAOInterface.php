<?php


namespace Src\Daos;

interface DAOInterface
{
    public function insert($entityObject);
    public function getAll();
    public function getOne($id);
    public function update($entityObject);
    public function delete($id);
}