<?php

namespace Src\Daos;

use Src\Entitys\Debts;
use Src\Lib\PDOConnection;
use PDO;

class DebtDAO implements DAOInterface
{
    public static $instance;

    public function __construct()
    {
    }

    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new DebtDAO();

        return self::$instance;
    }

    public function insert($entityObject)
    {
        try {
            $sql = "INSERT INTO debts (       
                description,
                value,
                due_date,
                updated,
                debtor_id
                ) 
                VALUES (
                :description,
                :value,
                :due_date,
                NOW(),
                :debtor_id
                )";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $values = array(
                ':description' => $entityObject->getDescription(),
                ':value' => $entityObject->getValue(),
                ':due_date' => $entityObject->getDueDate(),
                ':debtor_id' => $entityObject->getDebtor(),
            );

            return $p_sql->execute($values);

        } catch (\PDOException $e) {

            return false;

        }
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getOne($id)
    {
        try {
            $sql = "SELECT * FROM debts WHERE id = :id";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->bindValue(":id", $id);

            $p_sql->execute();

            $array = $p_sql->fetch(PDO::FETCH_ASSOC);

            return $this->createDebtFromArray($array);

        } catch (Exception $e) {

            return $e->getMessage();

        }
    }

    public function update($entityObject)
    {
        try {
            $sql = "UPDATE debts SET       
                        description = :description,
                        value = :value,
                        due_date = :due_date,
                        updated = NOW(),
                        debtor_id = :debtor_id
                        WHERE id = :id
                ";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $values = array(
                ':id' => $entityObject->getId(),
                ':description' => $entityObject->getDescription(),
                ':value' => $entityObject->getValue(),
                ':due_date' => $entityObject->getDueDate(),
                ':debtor_id' => $entityObject->getDebtor(),
            );

            return $p_sql->execute($values);

        } catch (\PDOException $e) {

            echo $e->getMessage();
            return false;

        }
    }

    public function delete($id)
    {
        try {

            $sql = "DELETE FROM debts WHERE id = :id";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->bindValue(":id", $id);

            $p_sql->execute();

            return $p_sql->execute();

        } catch (Exception $e) {
            return false;

        }
    }

    public function getDebtsFromDebtor($id){
        try {
            $sql = "SELECT * FROM debts WHERE debtor_id = :id";

            $p_sql = PDOConnection::getInstance()->prepare($sql);

            $p_sql->bindValue(":id", $id);

            $p_sql->execute();

            return $p_sql->fetchAll(PDO::FETCH_ASSOC);

        } catch (Exception $e) {

            return $e->getMessage();

        }
    }

    public function createDebtFromArray($array){
        $debt = new Debts(
            $array["id"],
            $array["description"],
            $array["value"],
            $array["due_date"],
            $array["updated"],
            $array["debtor_id"]
        );
        return $debt;
    }
}